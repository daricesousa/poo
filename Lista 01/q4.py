def fatorialIterativo(n, resultado): 
    while(n != 0):
        resultado *= n
        n -= 1 
    return resultado

def fatorialRecursivo(n, resultado):
    resultado *= n;
    n -= 1;
    if(n != 0):
        return fatorialRecursivo(n, resultado)
    else:
        return resultado


n = int(input())
resultado = 1
tipo = input("Digite 1 para fatorial iterativo e 2 para fatorial recursivo\n")
if(tipo == 1):
    resultado = fatorialIterativo(n, resultado)
else:
    resultado = fatorialRecursivo(n, resultado)
print(resultado)