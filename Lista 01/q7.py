n = int(input())
lista = []
binario = ''

while(n>=2):
    resto = n % 2
    n = n // 2
    lista.append(resto)
lista.append(n)

for algarismo in range(len(lista)-1, -1, -1):
    binario += str(lista[algarismo])

print(binario)