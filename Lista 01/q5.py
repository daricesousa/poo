def fatorial(n):
    resultado = 1   
    while(n != 0):
        resultado *= n
        n -= 1 
    return resultado

def arranjo(n, p):
    print(fatorial(n) / fatorial(n-p))

def combinacao(n, r):
    print(fatorial(n)/(fatorial(r)*fatorial(n-r)))

a = int(input())
b = int(input())

arranjo(a, b)
combinacao(a, b)