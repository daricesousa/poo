while(True):
    forma = int(input("Digite 1 para triangulo, 2 para quadrado e 3 para circulo\n"))
    if(forma == 1 or forma == 2 or forma == 3):
        if(forma == 1):
            base = float(input("Digite a medida da base\n"))
            altura = float(input("Digite a medida da altura\n"))
            area = base*altura/2
        elif(forma == 2):
            lado = float(input("Digite a medida do lado do quadrado\n"))
            area = lado*lado
        elif (forma == 3):
            raio = float(input("Digite a medida do raio\n"))
            area = 3.14*raio*raio
        print("A área é %.2f" %(area))

