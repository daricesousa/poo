from conta import Conta
from cliente import Cliente
from seguroDeVida import SeguroDeVida
from datetime import datetime
from historico import Historico

class ContaCorrente(Conta):
    def __init__(self):
        super().__init__()
        self._limite = 1000
        self._tributo = []
        self._historicoTransacoes = []

    def criarContaCorrente(cpf):
        cliente = Cliente.retornaClientePorCPF(cpf)
        if(cliente != False):
            if(cliente.contaCorrente == False):
                cliente.contaCorrente = ContaCorrente()
                print("Conta criada com sucesso")
                Conta.getConta(cliente.contaCorrente)
            else:
                print("Este cliente já possui uma conta corrente cadastrada")
    
    def getSaldo(cliente):
        return float(cliente.contaCorrente.saldo)

    def setSaldo(cliente, movimentacao):
        saldo = float(cliente.contaCorrente.saldo)
        saldo = saldo + float(movimentacao)
        cliente.contaCorrente.saldo = saldo      

    def adicionarTransacao(cliente,descricao, valor):
        cliente.contaCorrente._historicoTransacoes.append(str(datetime.now()) + ' ' +descricao + str(valor))

    def saque(cliente, valor):
        if(ContaCorrente.getSaldo(cliente) - float(valor) >= 0):
            ContaCorrente.adicionarTransacao(cliente,"Saque no valor de R$", valor)
            valor = - float(valor)
            ContaCorrente.setSaldo(cliente, valor)
            return True
        else:
            print("Saldo insuficiente")

    def deposito(cliente, valor):
        ContaCorrente.setSaldo(cliente, valor)
        ContaCorrente.adicionarTransacao(cliente,"Deposito no valor de R$", valor)

    def cobrarTributo(cpf):
        cliente = Cliente.retornaClientePorCPF(cpf)
        if(cliente != False):
            if(cliente.contaCorrente._tributo != False):
                tributoTotal = 0
                tributoTotal += 10 + ContaCorrente.getSaldo(cliente)*1/100
                tributoTotal += SeguroDeVida.valorMensalSoma(cliente)*2/100
                ContaCorrente.setSaldo(cliente, -tributoTotal)
                ContaCorrente.adicionarTransacao(cliente,"Tributo cobrado no valor de R$", tributoTotal)
                print("tributo cobrado com sucesso")
            else:
                print("A tributação está desativada para esse cliente")

   
    def imprimirHistorico(cliente):
            print('historico de transacoes:')
            for historico in cliente.contaCorrente._historicoTransacoes:
                print(historico)

    def retornaClientePorCPF(cpfBuscar):
        for pessoa in Cliente.clientes:
            cpf= pessoa.getCPF
            if(cpf == cpfBuscar):
                return pessoa
        print("Não há nenhum cliente cadastrado com esse CPF")
        return False

    def getContaCorrente(self):
        return self.getConta()

    def getNumeroCorrente(self):
        return self.getNumero()

    def desativarTributo(cliente):
        cliente.contaCorrente._tributo = False

    def exibirExtrato(cliente):
        Conta.getConta(cliente.contaCorrente)