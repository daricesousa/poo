from pessoa import Pessoa
from conta import Conta

class Cliente(Pessoa):
    clientes = []
    def __init__(self, nome, cpf, dataNascimento, profissao, renda):
        super().__init__(nome, cpf, dataNascimento)
        self._profissao = profissao
        self._renda = renda
        self.contaCorrente = False
        self.contaPoupanca = False
        self.seguroDeVida = []

    def cadastrarCliente(nome, cpf, dataNascimento, profissao, renda):
        Cliente.clientes.append(Cliente(nome, cpf, dataNascimento, profissao, renda))

    def getSeguroDeVida(cliente):
        return cliente.seguroDeVida

    def informacoesDoBanco():
        totalContas = 0
        for pessoa in Cliente.clientes:
            contas = 0
            pessoa.imprimirPessoa()
            print("profissao: %s\nrenda: %s" %(pessoa._profissao, pessoa._renda))
            if(pessoa.contaCorrente != False):
                contas = 1
            if(pessoa.contaPoupanca != False):
                contas += 1
            if(pessoa.seguroDeVida):
                print("O cliente possui", len(pessoa.seguroDeVida),"seguros de vida")
            print("O cliente possui %d contas cadastradas" %(contas))
            totalContas += contas
        print("\nTotal de contas cadastradas no banco:", totalContas)

                
    def retornaClientePorCPF(cpfBuscar):
        for pessoa in Cliente.clientes:
            cpf= pessoa.getCPF
            if(cpf == cpfBuscar):
                return pessoa
        print("Não há nenhum cliente cadastrado com esse CPF")
        return False

    def retornaClientePorNumeroDaConta(numeroBuscar):
        for pessoa in Cliente.clientes:
            if(pessoa.contaCorrente!=False):
                numeroCorrente = pessoa.contaCorrente.getNumeroCorrente()
                if(numeroCorrente == numeroBuscar):
                    return pessoa, 'corrente'
            if(pessoa.contaPoupanca!=False):
                numeroPoupanca = pessoa.contaPoupanca.getNumeroPoupanca()
                if(numeroPoupanca == numeroBuscar):
                    return pessoa, 'poupanca'
        print("Não há nenhuma conta cadastrada com esse numero")
        return False, False

 