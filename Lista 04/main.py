from funcionario import Funcionario
from cliente import Cliente
from contaCorrente import ContaCorrente
from contaPoupanca import ContaPoupanca
from seguroDeVida import SeguroDeVida


def cadastrarFuncionario():
    nome = input('Digite o nome')
    cpf = input('Digite o cpf')
    dataNascimento = input('Digite a data de nascimento')
    salario = input('Digite o salario')
    Funcionario.cadastrarFuncionario(nome, cpf, dataNascimento, salario)
    print('funcionario cadastrado com sucesso')

def cadastrarCliente():
    nome = input('Digite o nome')
    cpf = input('Digite o cpf')
    dataNascimento = input('Digite a data de nascimento')
    profissao = input('Digite a profissao')
    renda = input('digite a renda')
    Cliente.cadastrarCliente(nome, cpf, dataNascimento, profissao, renda)
    print('cliente cadastrado com sucesso')

def criarContaCorrente():
    cpf = input('cpf:')
    ContaCorrente.criarContaCorrente(cpf)

def criarContaPoupanca():
    cpf = input('cpf:')
    ContaPoupanca.criarContaPoupanca(cpf)

def criarSeguroDeVida():
    cpf = input('cpf:')
    valorMensal = input('valor mensal:')
    valorTotal = input('valor total:')
    SeguroDeVida.criarSeguroDeVida( cpf, valorMensal, valorTotal)

def cobrarTributo():
    cpf = input('cpf:')
    ContaCorrente.cobrarTributo(cpf)

def sacar():
    numero = input("Digite o numero da conta:")
    cliente,conta = Cliente.retornaClientePorNumeroDaConta(numero)
    if(cliente != False):
        valor = input("valor: ")
        if(conta == 'corrente'):
            if(ContaCorrente.saque(cliente, valor)):
                print("Saque realizado com sucesso")
        else:
            if(ContaPoupanca.saque(cliente, valor)):
                print("Saque realizado com sucesso")

def deposito():
    numero = input("Digite o numero da conta:")
    cliente,conta = Cliente.retornaClientePorNumeroDaConta(numero)
    if(cliente != False):
        valor = input("valor: ")
        if(conta == 'corrente'):
            ContaCorrente.deposito(cliente, valor)
        else:
            ContaPoupanca.deposito(cliente, valor)
        print("Deposito realizado com sucesso")
    
def imprimirHistorico():
    numero = input("Digite o numero da conta:")
    cliente,conta = Cliente.retornaClientePorNumeroDaConta(numero)
    if(cliente != False):
        if(conta == 'corrente'):
            ContaCorrente.imprimirHistorico(cliente)
        else:
            ContaPoupanca.imprimirHistorico(cliente)
def transferencia():
    numero = input("Digite o numero da sua conta:")
    cliente1,conta1 = Cliente.retornaClientePorNumeroDaConta(numero)
    numero = input("Digite o numero da conta que deseja depositar:")
    cliente2,conta2 = Cliente.retornaClientePorNumeroDaConta(numero)
    if(cliente1 != False and cliente2!= False):
        valor = input("valor: ")
        if(conta1 == 'corrente'):
            ok = ContaCorrente.saque(cliente1, valor)
        else:
            ok = ContaPoupanca.saque(cliente1, valor)
        if(ok):
            if(conta2 == 'corrente'):
                ContaCorrente.deposito(cliente2, valor)
            else:
                ContaCorrente.deposito(cliente2, valor)
            print("Transferencia realizada com sucesso")
        else:
            print('Erro na transferencia')

def informacoesDoBanco():
    Cliente.informacoesDoBanco()

def desativarTributo():
    numero = input("Digite o numero da conta:")
    cliente,conta = Cliente.retornaClientePorNumeroDaConta(numero)
    if(cliente != False):
        ContaCorrente.desativarTributo(cliente)
        print("Tributacao desativa com sucesso")

def exibirExtrato():
    numero = input("Digite o numero da conta:")
    cliente,conta = Cliente.retornaClientePorNumeroDaConta(numero)
    if(cliente != False):
        ContaCorrente.exibirExtrato(cliente)

while(True):
    print("************Menu:*******************")
    print("Escolha uma das opções:")
    print("1 - cadastrar funcionario")
    print("2 - cadastrar cliente")
    print("3 - criar conta corrente")
    print("4 - criar conta poupanca")
    print("5 - criar seguro de vida")
    print("6 - cobrar tributo")
    print("7 - sacar")
    print("8 - depositar")
    print("9 - transferir")
    print("10 - imprimir historico de transações")
    print("11 - exibir informações do banco")
    print("12 - desativar tributação")
    print("13 - exibir extrato")
    opcao = int(input())

    if(opcao == 1):
        cadastrarFuncionario()
    elif(opcao == 2):
        cadastrarCliente()
    elif(opcao == 3):
        criarContaCorrente()
    elif(opcao == 4):
        criarContaPoupanca()
    elif(opcao == 5):
        criarSeguroDeVida()
    elif(opcao == 6):
        cobrarTributo()
    elif(opcao == 7):
        sacar()
    elif(opcao == 8):
        deposito()
    elif(opcao == 9):
        transferencia()
    elif(opcao == 10):
        imprimirHistorico()
    elif(opcao == 11):
        informacoesDoBanco()
    elif(opcao == 12):
        desativarTributo()
    elif(opcao == 13):
        exibirExtrato()