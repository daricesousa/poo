from random import randint

class Conta:
    def __init__(self):
        self._numero = str(randint(10000, 99999))
        self.saldo = 0

    def getConta(self):
        print("Numero:", self._numero, "\nSaldo:", self.saldo);

    def getNumero(self):
        return self._numero
