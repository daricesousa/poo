from pessoa import Pessoa

class Funcionario(Pessoa):
    funcionarios = []
    def __init__(self, nome, cpf, dataNascimento, salario):
        super().__init__(nome, cpf, dataNascimento)
        self._salario = salario

    def cadastrarFuncionario(nome, cpf, dataNascimento, salario):
        Funcionario.funcionarios.append(Funcionario(nome, cpf, dataNascimento, salario))

    def imprimirFuncionario():
        for pessoa in Funcionario.funcionarios:
            print(pessoa._nome, pessoa._cpf, pessoa._dataNascimento, pessoa._salario)
