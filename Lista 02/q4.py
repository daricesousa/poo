# Faça uma função para retornar uma string ordenada sem usar a função de ordenação
# disponível.


def ordenarString(string):
    string = list(string)
    n = len(string)
    for i in range(len(string)):
        for j in range(0,n-1):
            if(string[j] > string[j+1]):
                aux = string[j];
                string[j] = string[j+1];
                string[j+1]= aux;
        n -= 1;
        i += 1;

    return ''.join(string)


print(ordenarString('darice'))