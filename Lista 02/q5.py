# Faça um joguinho em Python de adivinha. O seu programa vai gerar um número aleatório
# de 0 até 100, o número é oculto ao jogador. O jogador deverá acertar o número secreto. O
# jogo terminará se o usuário acertar ou se ele tentar mais de 10 vezes. Quando o usuário
# acertar o programa deverá perguntar se o jogador deseja repetir ou sair. Toda vez que o
# usuário informar um número o programa deverá informar qual é o número da tentativa.

from random import randint

def adivinha():
    numeroAleatorio = (randint(1,10))
    print("Tente adivinhar o número (entre 1 e 100)");
    for i in range(1,11):
        print("Tentativa", i, ":")
        numero = int(input())
        if(numero == numeroAleatorio):
            print("Você adivinhou!")
            return 0
    print("Você perdeu")

print("Jogo de adivinhação!")
jogarNovamente = 1
while(jogarNovamente == 1):
    adivinha()
    jogarNovamente = int(input("Digite 1 para jogar novamente\nDigite 2 para sair\n"))
