# Escreva um programa que cria uma lista de 10 posições e mostre-a. Em seguida, troque o
# primeiro elemento pelo o último, o segundo com o penúltimo, o terceiro com o antepenúltimo
# e, assim, sucessivamente. Mostre a nova lista após todas as trocas.

from random import randint

lista = []
for i in range(10):
    lista.append(randint(0,10))
print(lista)
n = len(lista)-1
for i in range(int(len(lista)/2)):
    aux = lista[i]
    lista[i] = lista[n]
    lista[n] = aux
    n -= 1
print(lista)
