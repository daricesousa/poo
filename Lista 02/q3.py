# Vimos em sala de aula o método find e index que retornam a posição de um caracter dentro
# de uma string. Por exemplo: “Teste”.find(“T”), a saída será 0. Implemente sua solução de
# procurar um caracter em uma string sem utilizar os métodos find ou o index.

def buscar(buscar, string):
    i=0
    for caracter in string:
        if(caracter == buscar):
            return i
        i += 1

print(buscar('a','darice'))