# Escreva um programa para armazenar uma agenda de telefones em um dicionário. Cada
# pessoa pode ter um ou mais telefones e a chave do dicionário é o nome da pessoa. Seu
# programa deve ter um menu com as seguintes funções:
# a. incluirNovoNome – essa função acrescenta um novo nome na agenda, com um ou
# mais telefones. Ela deve receber como argumentos o nome e os telefones.
# b. incluirTelefone – essa função acrescenta um telefone em um nome existente na
# agenda. Caso o nome não exista na agenda, você̂ deve perguntar se a pessoa
# deseja incluí-lo. Caso a resposta seja afirmativa, use a função anterior para incluir o
# novo nome.
# c. excluirTelefone – essa função exclui um telefone de uma pessoa que já está na
# agenda. Se a pessoa tiver apenas um telefone, ela deve ser excluída da agenda.
# d. excluirNome – essa função exclui uma pessoa da agenda.
# e. consultarTelefone – essa função retorna os telefones de uma pessoa na agenda.
# f.Quando o usuário digitar um número negativo o programa é encerrado

def incluirNovoNome(agenda, nome, telefone):
    if(nome==0 and telefone==0):
        nome = input("Digite o nome do contato: ")
        telefone = input("Digite o número do telefone: ")
    
    contato ={}
    contato[nome] = [telefone]
    agenda.append(contato)
    print("Nome incluido com sucesso\n")
   

def incluirTelefone(agenda):
    nome = input("Digite o nome do contato: ")
    telefone = input("Digite o número do telefone: ")
    posicao = posicaoNaAgenda(nome, agenda)
    if(posicao != -1):
        agenda[posicao][nome].append(telefone)
        print("Telefone incluido com sucesso\n")
       
    else:
        print("O contato", nome, "não pertence a sua agenda.\nDeseja adiciona-lo?\n1-sim\n2-não\n")
        opcao = int(input())
        if(opcao == 1):
            incluirNovoNome(agenda, nome, telefone)
    
def posicaoNaAgenda(contatoBuscar, agenda):
    posicao = 0
    for contato in agenda:
        for nome in contato:
            if(nome == contatoBuscar):
                return posicao
        posicao += 1
    return -1


def excluirNome(agenda):
    nome = input("Digite o nome do contato que deseja excluir: ")
    posicao = posicaoNaAgenda(nome, agenda)
    if(posicao == -1):
        print("Esse contato não pertence a lista\n")
    else:
        del(agenda[posicao])
        print("Nome excluido com sucesso\n")
   

def excluirTelefone(agenda):
    nome = input("Digite o nome do contato o qual o telefone que deseja excluir pertence: ")
    telefone = input("Digite o número do telefone que deseja excluir: ")
    posicao = posicaoNaAgenda(nome, agenda)
    if(posicao == -1):
        print("Esse contato não pertence a lista\n")
    else:
        for i in range(len(agenda[posicao][nome])):
            if(agenda[posicao][nome][i] == telefone):
                del(agenda[posicao][nome][i])
        print("Telefone excluido com sucesso\n")
        if(len(agenda[posicao][nome]) == 0):
            del(agenda[posicao])
   
def consultarTelefone(agenda):
    nome = input("Digite o nome do contato que deseja buscar: ")
    posicao = posicaoNaAgenda(nome, agenda)
    if(posicao == -1):
        print("Esse contato não pertence a lista\n")
    else:
        for i in range(len(agenda[posicao][nome])):
            print(agenda[posicao][nome][i])
        print("\n")

agenda = []

opcao = 0
while(opcao > -1):
    print("Escolha uma das opcões: ")
    print("1- incluir novo nome ")
    print("2- incluir telefone")
    print('3 - excluir telefone')
    print('4 - excluir nome')
    print('5 - consultar telefones')

    opcao = int(input())

    if(opcao == 1):
        incluirNovoNome(agenda, 0,0)
    elif(opcao == 2):
        incluirTelefone(agenda)
    elif(opcao == 3):
        excluirTelefone(agenda)
    elif(opcao == 4):
        excluirNome(agenda)
    elif(opcao == 5):
        consultarTelefone(agenda)






