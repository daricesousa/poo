# Escreva um programa que lê duas notas de vários alunos e armazena tais notas em um
# dicionário, onde a chave é o nome do aluno. Escreva uma função que retorna a média do
# aluno, dado seu nome.


def mediaAluno(nomeAluno, diario):
    for aluno in diario:
        for key in aluno:
             if key == nomeAluno:
                 media = (aluno[key][0] + aluno[key][1])/2
                 return media


diario = []

n = int(input("Digite a quantidade de alunos: "))

for i in range(n):
    dicionario = {}
    print("\nAluno ", i+1, ":")
    nome = input("Nome: ")
    nota1 = float(input("Nota1: "))
    nota2 = float(input("Nota 2: "))
    dicionario[nome] = [nota1, nota2]
    diario.append(dicionario)


buscarAluno = input("\nDigite o nome do aluno pelo qual deseja buscar: ")
print("media:", mediaAluno(buscarAluno, diario))

