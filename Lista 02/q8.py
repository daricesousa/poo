# Gere uma lista de treze elementos inteiros, que é o gabarito de um teste da loteria esportiva,
# contendo os números 1, 2 ou 3 em cada posição. Também, gere 3 cartões de aposta
# representando um cartão de um apostador que contem o número do seu cartão e um vetor
# de respostas com treze posições. Verifique para cada apostador o número de acertos,
# comparando a lista de gabarito com a lista de respostas. Escreva o número do apostador e
# o número de acertos. Se o apostador tiver treze acertos, mostre a mensagem "Ganhador".
from random import randint

def gerarCartao():
    lista = []
    for i in range(13):
        lista.append(randint(1,3))
    return lista

def contarAcertos(cartao, respostas):
    acertos = 0
    for i in range(13):
        if(cartao[i] == respostas[i]):
            acertos += 1
    if(acertos == 13):
        print("O ganhador foi:")
    return acertos




cartao1 = gerarCartao()
cartao2 = gerarCartao()
cartao3 = gerarCartao()
respostas = gerarCartao()

print("Cartao 1:", contarAcertos(cartao1,respostas), "acertos")
print("Cartao 2:", contarAcertos(cartao2,respostas), "acertos")
print("Cartao 3:", contarAcertos(cartao1,respostas), "acertos")


