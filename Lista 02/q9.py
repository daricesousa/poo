# Construa um algoritmo para o funcionamento de uma agenda. Devem ser lidos os seguintes
# dados de 10 pessoas: nome, endereço, CEP, bairro e telefone. Tais dados devem ser
# armazenados na agenda, cuja representação é uma lista com 10 linhas (referentes às
# pessoas) e 5 colunas (referentes aos dados). Por fim, o algoritmo deve gerar como saída a
# agenda impressa em ordem invertida em relação à ordem de entrada dos dados.


agenda = []

for i in range(10):
    dicionario = {}
    dicionario['Nome'] = input("Nome: ")
    dicionario['Endereco'] = input("Endereço:")
    dicionario['CEP'] = input('CEP: ')
    dicionario['Bairro'] = input("Bairro: ")
    dicionario['Telefone'] = input("Telefone: ")
    agenda.append(dicionario)

agenda.reverse()
print(agenda)