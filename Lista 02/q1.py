# Aprendemos que a função len() retorna o tamanho de uma string. Faça uma função
# chamada de tamString(s) que recebe uma string qualquer e retorna o tamanho de “s” sem
# usar a função len().

def tamString(s):
    quant = 0;
    for caractere in s:
        quant += 1
    return quant

string = input()

print(tamString(string))