# Crie uma função que imprima o conteúdo de uma Lista. Por exemplo, se o array for [1, “a”,
# 2.5, True], então será impresso em cada linha os valores: 1, “a”, 2.5 e True. Construa uma
# solução que trate a possibilidade que um elemento do vetor também possa ser um vetor, por
# exemplo: [0,[1,2],3], neste caso a saída do programa em cada linha será: 0,1,2,3. Sua
# solução está tratando desta possibilidade: [0,1,[2,[3,4]],5] ?

def conteudoDoArray(array):
    for unidade in array:
        if(type(unidade) == list):
            conteudoDoArray(unidade)
        else:
            print(unidade)
       

lista = [0,1,[2,[3,4]],5]
conteudoDoArray(lista)
