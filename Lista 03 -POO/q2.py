'''
a. armazenaPessoa(nome, idade, altura);
b. removePessoa(nome);
c. buscaPessoa(nome); // informa em que posição da agenda está a pessoa
d. imprimeAgenda(); // imprime os dados de todas as pessoas da agenda
e. imprimePessoa(index); // imprime os dados da pessoa que está na posição “i” da agenda.
'''
from pessoa import Pessoa


class Agenda:
    agendaPessoas = []

    def __init__(self,nome,idade,altura):
        self.nome = nome;
        self.idade = idade;
        self.altura = altura;

    @staticmethod
    def armazenaPessoa(nome, idade, altura):
        Agenda.agendaPessoas.append(Agenda(nome, idade, altura))

    @staticmethod
    def imprimeAgenda():
        for pessoa in Agenda.agendaPessoas:
            print(pessoa.nome, pessoa.idade, pessoa.altura)

    @staticmethod   
    def removePessoa(nome):
        posicao = Agenda.buscaPessoa(nome)
        del Agenda.agendaPessoas[posicao]

    @staticmethod   
    def buscaPessoa(nome):
        posicao = 0
        for pessoa in Agenda.agendaPessoas:
            if(pessoa.nome == nome):
                return posicao
            posicao += 1

    @staticmethod
    def imprimePessoa(index):
        pessoa = Agenda.agendaPessoas[index]
        print(pessoa.nome, pessoa.idade, pessoa.altura)
        



