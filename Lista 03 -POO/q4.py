from televisao import Televisao
from controleRemoto import ControleRemoto

t = Televisao(20,1)
c = ControleRemoto(t)


def menu():
    print()
    print("Escolha uma opcao:")
    print("1-aumentar volume")
    print("2-diminuir volume")
    print("3-aumentar canal")
    print("4-diminuir canal")
    print("5-ir para um canal especifico")
    print("6-consultar volume")
    print("7-consultar canal")

while(True):
    menu()
    op = int(input())
    if(op == 1):
        c.aumentarVolume()
    elif(op == 2):
        c.diminuirVolume()
    elif(op == 3):
        c.aumentarCanal()
    elif(op == 4):
        c.diminuirCanal()
    elif(op == 5):
        canal = int(input("Digite o número do canal: "))
        c.irParaCanal(canal)
    elif(op == 6):
        c.consultarVolumeAtual()
    elif(op == 7):
        c.consultarCanalAtual()

        
        