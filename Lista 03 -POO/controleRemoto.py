class ControleRemoto:
    def __init__(self, televisao):
        self.televisao = televisao


    def aumentarVolume(self):
        self.televisao.volume += 1

    def diminuirVolume(self):
        if(self.televisao.volume == 0):
            print("O som já está mudo")
        else:
            self.televisao.volume -= 1
    
    def aumentarCanal(self):
        self.televisao.canal += 1

    def diminuirCanal(self):
        if(self.televisao.canal == 1):
            print("impossivel diminuir mais")
        else:
            self.televisao.canal -= 1

    def consultarVolumeAtual(self):
        print("Volume:",self.televisao.volume)

    def consultarCanalAtual(self):
        print("Canal:",self.televisao.canal)

    def irParaCanal(self, canal):
        if(canal>0):
            self.televisao.canal = canal
        else:
            print("impossivel completar a ação")
    

    

    

